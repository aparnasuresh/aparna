#include<stdlib.h>
#include<stdio.h>
#include<assert.h>
#include<string.h>
#include<symtable.h>
void main() {

	SymTable_t head = SymTable_new();
	void * v;
	SymTable_put(&head,"aparna","207");
	SymTable_put(&head,"aneesha","206");
	SymTable_put(&head,"arthi","208");
	SymTable_put(&head,"priyadharsini","244");
	SymTable_put(&head,"sahithya","255");
	SymTable_put(&head,"sowmya","262");
	v=Sym_Table_get(head,"aparna");
	printf("\n the value is %s",v);
	printf("\n length: %d",Sym_Table_getLength(head));
	SymTable_map(head,Sym_Table_printlist,NULL);  
	SymTable_replace(head,"aparna","107");
	SymTable_print(head);
	SymTable_free(head);
    SymTable_print(head);
	getch();

}