#include<conio.h>
#include<stdio.h>
#include<assert.h>
#include<stdlib.h>
#include<string.h>
#include<symtable.h>
struct symtable {
   char *Key;
   void * Value;
   struct symtable * next;
};
typedef struct symtable* SymTable_t;

SymTable_t Sym_Table_new(void)
{
SymTable_t current=NULL;
if((current=(SymTable_t)malloc(sizeof(SymTable_t)))==NULL)
return NULL;
current->Key=NULL;
current->Value=NULL;
current->next=NULL;
return current;	
}

void SymTable_free(SymTable_t oSymTable)
{
	
	if(oSymTable == NULL)
		return;
	while (oSymTable!= NULL)
	{
		Sym_Table_t temp = oSymTable;
		temp->next=NULL;
		free(temp->Key);
		free((void*)temp->Value);
		free(temp);
		oSymTable=oSymTable->next;
	}
}


int SymTable_getLength (SymTable_t oSymTable)
{

    assert(oSymTable!= NULL);
    int length = 0;
    while(oSymTable !=NULL )
    {
    length++;
    oSymTable =oSymTable->next;
    }
    return length; 	
}

void SymTable_map(SymTable_t oSymTable, void (*pfApply)(const char *pcKey,const void *pvValue, void *pvExtra), const void *pvExtra)
{
   assert(oSymTable != NULL);
   assert(pfApply != NULL);
   SymTable_t temp = oSymTable;
   (*pfApply)((char*)(temp->Key),(void*)(temp->Value), (void*)pvExtra);
   while(temp->next != NULL)
   {
		temp= temp->next;
		(*pfApply)((char*)(temp->Key),(void*)(temp->Value), (void*)pvExtra);
   }
}


int SymTable_contains(SymTable_t oSymTable,const char *pcKey)
{
if(oSymTable==NULL)
return 0;
else
{
	while(oSymTable!=NULL)
	{
		if(strcmp(oSymTable->Key,pcKey)==0)
		return 1;
		oSymTable=oSymTable-> next;
	}
	return 0;
}
}
int SymTable_put (SymTable_t *oSymTable, const char *pcKey,const void *pvValue)
{
	if(SymTable_contains(*oSymTable,pcKey) == 0)
	{
		if(*oSymTable == NULL || (*oSymTable)->Key == NULL)
		{
			(*oSymTable) = (SymTable_t)malloc(sizeof(SymTable_t));
			(*oSymTable)->next = NULL;
			(*oSymTable)->Key = (char*)malloc(strlen(pcKey));
			(*oSymTable)->Value = (char*)malloc(strlen((char*)pvValue));
			strcpy((*oSymTable)->Key,pcKey);
			strcpy((*oSymTable)->Value,(char*)pvValue);
			return 1;
		}
		else
		{
		SymTable_t temp = *oSymTable, newnode;
		newnode = (SymTable_t)malloc(sizeof(SymTable_t));
		newnode->next = NULL;
		newnode->Key = (char*)malloc(strlen(pcKey));
		strcpy((newnode)->Key,pcKey);
		newnode->Value = (char*)malloc(strlen((char*)pvValue));
		strcpy((newnode)->Value,(char*)pvValue);
		while (temp->next != NULL)
		{
			temp = temp->next;
		}
		temp->next = newnode;
		return 1;
		}
	}
	return 0;
} 

void *SymTable_get (SymTable_t oSymTable, const char *pcKey)
{
	if(oSymTable==NULL)
	return NULL;
	else
	{
		while(oSymTable!=NULL)
	{
		if(strcmp(oSymTable->Key,pcKey)==0)
		return (void *)oSymTable->Value;
		oSymTable=oSymTable-> next;
	}
		return NULL;
	}
}

void* SymTable_remove(SymTable_t *oSymTable, const char *pcKey)
{
	SymTable_t temp, prev;
	temp=*oSymTable;

    while(temp!=NULL)
    {
		if(strcmp(temp->Key,pcKey)==0)
    {
		if(temp==*oSymTable)
        {
			(*oSymTable)=temp->next;
			return NULL;
        }
        else
        {
			prev->next=temp->next;
			return NULL;
        }
    }
    else   
    {
        prev=temp;
        temp= temp->next;  
    }
    }
	return NULL;
}

void *SymTable_replace(SymTable_t oSymTable, const char *pcKey, const void *pvValue)
{
	SymTable_t temp = oSymTable;
	if(temp == NULL)
		return NULL;
	else
	{
		if(strcmp(temp->Key,pcKey) == 0)
		{
			strcpy((char*)temp->Value, (char*)pvValue); 
			return temp->Value;
			
		}
		while(temp->next != NULL)
		{
			temp= temp->next;
			if(strcmp(temp->Key,pcKey)==0)
			{
				strcpy((char*)temp->Value, (char*)pvValue);
			 	return temp->Value;
			}
			
		}
		return NULL;
	}
}

void SymTable_print(SymTable_t oSymTable)

	SymTable_t temp = oSymTable;
	if(temp == NULL)
		printf("\n no elements");
	else
	{
		printf("\nThe keys are \n%s", oSymTable->Key);
		printf("\t%s", temp->Value);
		while(temp->next != NULL)
		{
			temp= temp->next;
				printf("\n%s", temp->Key);
				printf("\t%s", temp->Value);
		}
	}
}
void SymTable_printlist(const char*pcKey, const void *pvValue,void *pvExtra)
{
	printf("\n\n Key is %s",pcKey);
	printf("\n Value is %s", pvValue);
}